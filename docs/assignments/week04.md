<p><strong><span style="font-size: 24pt;">4. Corte controlado por ordenador</span></strong></p>
<p>Esta actividad se realizo con el uso de dos equipos distintos, para ello se separo en 2 tareas:</p>
<ul>
    <li>Equipo de Corte por Laser</li>
    <li>Equipo de Corte de Vinilo</li>
</ul>
<p>&nbsp;</p>
<p><strong><span style="font-size: 18pt;">Equipo de corte por l&aacute;ser</span></strong></p>
<p>Para la ejecucion de esta actividad haremos uso del equipo de corte por l&aacute;ser HSG HS-Z1390.</p>
<table style="border-collapse: collapse; width: 100%; height: 546px;" border="1">
    <tbody>
        <tr style="height: 29px;">
            <td style="width: 99.9558%; height: 29px; background-color: #3598db; text-align: center;" colspan="3"><strong>ESPECIFICACI&Oacute;N ESENCIAL DEL EQUIPO</strong></td>
        </tr>
        <tr style="height: 14px;">
            <td style="width: 14.7552%; height: 14px; background-color: #c2e0f4;">Velocidad de Corte</td>
            <td style="width: 22.9199%; height: 14px;">60 m/min</td>
            <td style="width: 62.2807%; height: 517px; text-align: center;" rowspan="17"><img src="../images/week03/207039-13967013.jpg" alt="207039-13967013-2.jpg" width="585" height="427" data-api-endpoint="../images/week03/207039-13967013.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Modo de enfriamiento</td>
            <td style="width: 22.9199%; height: 29px;">L&iacute;quido refrigerador</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Material aplicable</td>
            <td style="width: 22.9199%; height: 29px;">Acr&iacute;lico, Cristal, Vidrio, De cuero, MDF, Papel</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">&Aacute;rea de corte</td>
            <td style="width: 22.9199%; height: 29px;">1300*900 mm</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;" width="208">Lugar del origen</td>
            <td style="width: 22.9199%; height: 29px;">Guangdong, China</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Uso</td>
            <td style="width: 22.9199%; height: 29px;">Corte a l&aacute;ser</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Formato gr&aacute;fico compatible</td>
            <td style="width: 22.9199%; height: 29px;">AI, PLT, DXF, BMP, Dst, Dwg, LAS, DXP, CDR</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Marca</td>
            <td style="width: 22.9199%; height: 29px;">HSG LASER</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Poder (W)</td>
            <td style="width: 22.9199%; height: 29px;">60-150</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Potencia total (W)</td>
            <td style="width: 22.9199%; height: 29px;">&le;1250&nbsp;</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Alimentaci&oacute;n</td>
            <td style="width: 22.9199%; height: 29px;">AC220V&plusmn;10% 60HZ</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Volumen (m3)</td>
            <td style="width: 22.9199%; height: 29px;">2.7</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Peso (kg)</td>
            <td style="width: 22.9199%; height: 29px;">350</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Dimensi&oacute;n exterior (mm)</td>
            <td style="width: 22.9199%; height: 29px;">1820&times;1380&times;1050</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Precisi&oacute;n</td>
            <td style="width: 22.9199%; height: 29px;">&lt;0.02 mm</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 14.7552%; height: 29px; background-color: #c2e0f4;">Software</td>
            <td style="width: 22.9199%; height: 29px;">V320 \ V430 \ V6332G</td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 14.7552%; height: 10px; background-color: #c2e0f4;">Nombre Comercial</td>
            <td style="width: 22.9199%; height: 10px;">Co2 80w Maquina De Corte A Laser</td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<p>Realizando una busqueda en internet se encontro una tabla de parametros recomendados para corte por laser en distintos materiales</p>
<table style="border-collapse: collapse; width: 60%; height: 475px;" border="1">
    <tbody>
        <tr style="height: 29px;">
            <td style="width: 16.8217%; height: 29px; text-align: center; background-color: #3598db;"></td>
            <td style="width: 49.8007%; height: 29px; text-align: center; background-color: #3598db;" colspan="3"><strong>CORTE</strong></td>
            <td style="width: 33.3112%; height: 29px; text-align: center; background-color: #3598db;" colspan="2"><strong>GRABADO</strong></td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 16.8217%; height: 29px; text-align: center; background-color: #3598db;"><strong>Material</strong></td>
            <td style="width: 12.9858%; height: 29px; text-align: center; background-color: #3598db;"><strong>Espesor (mm)</strong></td>
            <td style="width: 20.1593%; height: 29px; text-align: center; background-color: #3598db;"><strong>Velocidad (mm/min)</strong></td>
            <td style="width: 16.6556%; height: 29px; text-align: center; background-color: #3598db;"><strong>Rango de Potencia</strong></td>
            <td style="width: 16.6556%; height: 29px; text-align: center; background-color: #3598db;"><strong>Velocidad (mm/min)</strong></td>
            <td style="width: 16.6556%; height: 29px; text-align: center; background-color: #3598db;"><strong>Rango de Potencia</strong></td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 16.8217%; height: 98px; text-align: center; background-color: #c2e0f4;" rowspan="4">MDF</td>
            <td style="width: 12.9858%; height: 29px; text-align: center;">3</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">15-20</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">50-60</td>
            <td style="width: 16.6556%; height: 98px; text-align: center;" rowspan="4">200-400</td>
            <td style="width: 16.6556%; height: 98px; text-align: center;" rowspan="4">30-50</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 12.9858%; height: 29px; text-align: center;">6</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">10-15</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">60-70</td>
        </tr>
        <tr style="height: 11px;">
            <td style="width: 12.9858%; height: 11px; text-align: center;">9</td>
            <td style="width: 20.1593%; height: 11px; text-align: center;">5-8</td>
            <td style="width: 16.6556%; height: 11px; text-align: center;">80-85</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 12.9858%; height: 29px; text-align: center;">12</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">3-8</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">85-90</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 16.8217%; height: 145px; text-align: center; background-color: #c2e0f4;" rowspan="5">Acrilico</td>
            <td style="width: 12.9858%; height: 29px; text-align: center;">3</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">10-20</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">60-65</td>
            <td style="width: 16.6556%; height: 145px; text-align: center;" rowspan="5">200-400</td>
            <td style="width: 16.6556%; height: 145px; text-align: center;" rowspan="5">20-35</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 12.9858%; height: 29px; text-align: center;">6</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">6-10</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">60-70</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 12.9858%; height: 29px; text-align: center;">9</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">5-7</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">80-85</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 12.9858%; height: 29px; text-align: center;">12</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">2-5</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">92-93</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 12.9858%; height: 29px; text-align: center;">18</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">1-2</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">85-90</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 16.8217%; height: 29px; text-align: center; background-color: #c2e0f4;">Papel Bond</td>
            <td style="width: 12.9858%; height: 29px; text-align: center;">NA</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">100</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">15-20</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">600</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">15-20</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 16.8217%; height: 29px; text-align: center; background-color: #c2e0f4;">Cartulina</td>
            <td style="width: 12.9858%; height: 29px; text-align: center;">NA</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">50</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">30-35</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">600</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">40-45</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 16.8217%; height: 29px; text-align: center; background-color: #c2e0f4;">Tactopiel</td>
            <td style="width: 12.9858%; height: 29px; text-align: center;">NA</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">20</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">20-25</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">500</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">15-20</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 16.8217%; height: 29px; text-align: center; background-color: #c2e0f4;">Tela</td>
            <td style="width: 12.9858%; height: 29px; text-align: center;">NA</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">30</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">20-25</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">500</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">12.5-17.5</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 16.8217%; height: 29px; text-align: center; background-color: #c2e0f4;">Vidrio</td>
            <td style="width: 12.9858%; height: 29px; text-align: center;">NA</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">NA</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">NA</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">300</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">30-35</td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 16.8217%; height: 29px; text-align: center; background-color: #c2e0f4;">Carton Corrugado</td>
            <td style="width: 12.9858%; height: 29px; text-align: center;">NA</td>
            <td style="width: 20.1593%; height: 29px; text-align: center;">50</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">50-65</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">400</td>
            <td style="width: 16.6556%; height: 29px; text-align: center;">15-30</td>
        </tr>
    </tbody>
</table>
<p>Para la ejecucion de la tarea se realizaron diferentes pruebas con el proposito de identificar la precisi&oacute;n de corte del equipo en diferentes materiales.</p>
<p><strong><span style="font-size: 14pt;">Determinar el punto focal</span></strong></p>
<table style="border-collapse: collapse; width: 100%;" border="0" width="160" cellspacing="0" cellpadding="0">
    <tbody>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 20%;" width="20%">Hacemos uso de una regla graduada de forma triangular para medir y modificar la altura del cabezal</td>
            <td style="width: 80%;" width="80"><img src="../images/week03/t3-00.jpg" alt="t3-00.jpg" width="200" height="99" data-api-endpoint="../images/week03/t3-00.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 20%;">Medimos la altura del cabezal, la cual iremos variando en razon de 1 mm.</td>
            <td style="width: 80%;"><img src="../images/week03/t3-01.jpg" alt="t3-01.jpg" width="200" height="150" data-api-endpoint="../images/week03/t3-01.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 20%;" rowspan="2">
                <p>Modificaremos la altura del cabezal empezando desde 2mm hasta alcanzar los 10 mm.</p>
                <p>Se utilizo plancha de MDF de 5mm de espesor</p>
                <p>Se evalua los resultados en la parte frontal y posterior de la plancha&nbsp;</p>
            </td>
            <td style="width: 40%;"><img src="../images/week03/t3-02.jpg" alt="t3-02.jpg" width="640" height="175" data-api-endpoint="h../images/week03/t3-02.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="width: 40%;"><img src="../images/week03/t3-03.jpg" alt="t3-03.jpg" width="640" height="153" data-api-endpoint="../images/week03/t3-03.jpg" data-api-returntype="File" /></td>
        </tr>
    </tbody>
</table>
<p>Se observ&oacute; que entre 4 mm y 5&nbsp;mm, la parte superior y posterior del agujero ten&iacute;an&nbsp;una forma m&aacute;s&nbsp;redondeada; luego de&nbsp;comparar&nbsp;los dos agujeros&nbsp;se&nbsp;concluy&oacute;&nbsp;que a 4 mm&nbsp;de altura&nbsp;el corte ideal ser&iacute;a el de menor di&aacute;metro y&nbsp;agujero&nbsp;m&aacute;s&nbsp;redondo.</p>
<p>&nbsp;</p>
<p><span style="font-size: 14pt;"><strong>Calibraci&oacute;n de la potencia y velocidad de corte</strong></span></p>
<p>Para esta parte del trabajo se hara uso del programa RDworks.</p>
<p>Este programa permite seleccionar las lineas del dibujo y asignarles colores para diferenciarlas, a su vez poder dar a cada color parametros de velocidad y potencia.&nbsp; &nbsp; La convinacion e estos parametros permite realizar corte o grabado.</p>
<table style="border-collapse: collapse; width: 100%;" border="0" width="160" cellspacing="0" cellpadding="0">
    <tbody>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 1323px;" colspan="3" width="80"><strong>Prueba de MDF 4mm</strong></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 555px;">
                <p>Dise&ntilde;o para el corte de MDF 4mm con;</p>
                <ul>
                    <li>velocidades de 10, 35, 60, 60, 85 y 110 mm/s</li>
                    <li>potencias de 10%, 30%, 45%, 60% y 80%.</li>
                </ul>
            </td>
            <td style="width: 464px;"><img src="../images/week03/t3-08.jpg" alt="t3-08.jpg" width="640" height="328" data-api-endpoint="../images/week03/t3-08.jpg" data-api-returntype="File" /></td>
            <td style="width: 304px;"><img src="../images/week03/t3-09.jpg" alt="t3-09.jpg" width="282" height="320" data-api-endpoint="../images/week03/t3-09.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 1323px;" colspan="3">Se observa que el corte se consigue con la velocidad m&aacute;s baja, que es de 10 mm/s y con potencias del 45% en adelante.</td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 1323px;" colspan="3"></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 1323px;" colspan="3"><strong>Prueba de MDF 5mm</strong></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 555px;">
                <p>Dise&ntilde;o para cortar MDF 5mm:</p>
                <ul>
                    <li>velocidades de 10, 35, 60, 60, 85 y 110 mm/s</li>
                    <li>potencias de 10%, 30%, 45%, 60% y 80%.</li>
                </ul>
            </td>
            <td style="width: 464px;"><img src="../images/week03/t3-04.jpg" alt="t3-04.jpg" width="640" height="328" data-api-endpoint="../images/week03/t3-04.jpg" data-api-returntype="File" /></td>
            <td style="width: 304px;"><img src="../images/week03/t3-05.jpg" alt="t3-05.jpg" width="279" height="320" data-api-endpoint="../images/week03/t3-05.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 1323px;" colspan="3">Se observa que el corte se consigue con la velocidad m&aacute;s baja, que es de 10 mm/s y con una potencia alta del 80%</td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 1323px;" colspan="3"></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 1323px;" colspan="3"><strong>Prueba de ACRYLICO 4mm</strong></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 555px;">
                <p>Dise&ntilde;o para el corte de ACRYLIC 4mm.</p>
                <ul>
                    <li>velocidades de 10, 35, 60, 60, 85 y 110 mm/s</li>
                    <li>potencias de 10%, 30%, 45%, 60% y 80%</li>
                </ul>
            </td>
            <td style="width: 464px;"><img src="../images/week03/t3-06.jpg" alt="t3-06.jpg" width="640" height="328" data-api-endpoint="../images/week03/t3-06.jpg" data-api-returntype="File" /></td>
            <td style="width: 304px;"><img src="../images/week03/t3-07.jpg" alt="t3-07.jpg" width="277" height="320" data-api-endpoint="../images/week03/t3-07.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 1323px;" colspan="3">Se observa que el corte se consigue con la velocidad m&aacute;s baja, que es de 10 mm/s y con potencias del 45% en adelante</td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<p><span style="font-size: 14pt;"><strong>Calibraci&oacute;n de la potencia y velocidad de corte para encaje de piezas a presion</strong></span></p>
<table style="border-collapse: collapse; width: 100%; height: 118px;" border="0" width="160" cellspacing="0" cellpadding="0">
    <tbody>
        <tr style="height: 15.0pt;">
            <td style="height: 15px; width: 1332px;" colspan="3" width="80"><strong>Prueba de ajuste a presi&oacute;n en MDF 3mm</strong></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 58px; width: 410px;">Se dise&ntilde;&oacute; un boceto con las dimensiones de prueba en el software Fusion 360&nbsp;</td>
            <td style="width: 474px; height: 29px;"><img src="../images/week03/t3-10.jpg" alt="t3-10.jpg" width="400" height="225" data-api-endpoint="../images/week03/t3-10.jpg" data-api-returntype="File" /></td>
            <td style="width: 448px; height: 29px;"><img src="../images/week03/t3-11.jpg" alt="t3-11.jpg" width="400" height="225" data-api-endpoint="../images/week03/t3-11.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15px; width: 410px;">Se configur&oacute; en RDworks para ser cortado en el equipo de corte por l&aacute;ser</td>
            <td style="width: 474px; height: 15px;"><img src="../images/week03/t3-12.jpg" alt="t3-12.jpg" width="400" height="225" data-api-endpoint="../images/week03/t3-12.jpg" data-api-returntype="File" /></td>
            <td style="width: 448px; height: 15px;"><img src="../images/week03/t3-13.jpg" alt="t3-13.jpg" width="400" height="105" data-api-endpoint="../images/week03/t3-13.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15px; width: 1332px;" colspan="3">El resultado de la prueba fue que el mejor ajuste a presi&oacute;n en MDF de 3 mm es en la dimensi&oacute;n de 2,7 mm.</td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<p><span style="font-size: 14pt;"><strong>Dise&ntilde;o de Lampara de noche en acrilico con cortadora laser</strong></span></p>
<p><span style="font-size: 12pt;">Se realizo el dise&ntilde;o de una lampara de noche con los logotipos de heroes de Avengers, la herramienta de dise&ntilde;o que se utilizo es el programa AutoCad ya que permite mayor control de las dimensiones y variedad de formatos de archivos compatibles con el programa RDworks.</span></p>
<table style="border-collapse: collapse; width: 100%;" border="0" width="160" cellspacing="0" cellpadding="0">
    <tbody>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 33.33%;" width="33.33%">Dise&ntilde;o de las piezas en AutoCad</td>
            <td style="width: 60%;" colspan="2"><img src="docs\images\week03\lampara.png" alt="lampara.png" width="400" height="216" data-api-endpoint="../images/week03/lampara.png" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 33.33%;">Configuracion de los parametros de corte</td>
            <td style="width: 60%;" colspan="2"><img src="../images/week03/lamp02.jpg" alt="lamp02.jpg" width="400" height="216" data-api-endpoint="../images/week03/lamp02.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 33.33%;">Simulacion en el programa RDworks antes de llevar a la cortadora laser</td>
            <td style="width: 60%;" colspan="2"><img src="../images/week03/lamp03.jpg" alt="lamp03.jpg" width="400" height="191" data-api-endpoint="../images/week03/lamp03.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 33.33%;" rowspan="2">Proceso de Corte</td>
            <td style="width: 33.33%;"><img src="../images/week03/IMG_20230804_171415.jpg" alt="IMG_20230804_171415.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230804_171415.jpg" data-api-returntype="File" /></td>
            <td style="width: 33.33%;"><img src="../images/week03/IMG_20230804_171921.jpg" alt="IMG_20230804_171921.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230804_171921.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr>
            <td style="width: 33.33%;"><img src="../images/week03/IMG_20230804_173200.jpg" alt="IMG_20230804_173200.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230804_173200.jpg" data-api-returntype="File" /></td>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230804_173943.jpg" alt="IMG_20230804_173943.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230804_173943.jpg" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 100%;" colspan="3">Proceso de Ensamblaje</td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 33.33%;">
                <p><img style="font-family: inherit; font-size: 1rem;" src="../images/week03/IMG_20230812_122415.jpg" alt="IMG_20230812_122415.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_122415.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230812_122429.jpg" alt="IMG_20230812_122429.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_122429.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230812_122614.jpg" alt="IMG_20230812_122614.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_122614.jpg" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230812_122753.jpg" alt="IMG_20230812_122753.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_122753.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230812_122829.jpg" alt="IMG_20230812_122829.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_122829.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230812_122925.jpg" alt="IMG_20230812_122925.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_122925.jpg" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230812_123518.jpg" alt="IMG_20230812_123518.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_123518.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230812_124012.jpg" alt="IMG_20230812_124012.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_124012.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230812_124358.jpg" alt="IMG_20230812_124358.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_124358.jpg" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230812_124920.jpg" alt="IMG_20230812_124920.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_124920.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 33.33%;">
                <p><img src="../images/week03/IMG_20230812_130227.jpg" alt="IMG_20230812_130227.jpg" width="400" height="180" data-api-endpoint="../images/week03/IMG_20230812_130227.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 33.33%;">
                <p>&nbsp;</p>
            </td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong><span style="font-size: 18pt;">Equipo de corte de vinilo</span></strong></p>
<p>En esta actividad se hara uso del equipo de corte de vinilo Roland GX-24.</p>
<p>Se plantea realizar un dise&ntilde;o que identifique los 5 procesos que forman parte de la metodologia del DESING THINKING, los dise&ntilde;os se realizaron en el programa AutoCad, se configuraron las dibujos en el programa CutStudio cada uno por separado para obtener imagenes de colores distintos, se procedi&oacute; a realizar el corte con el equipo de corte de vinilo y finalmente se elaboro una base de acrilico blanco para montar las imagenes, Se adiciono al final las palabras DESING THINKING para completar la idea.</p>
<table style="border-collapse: collapse; width: 100%;" border="0" width="160" cellspacing="0" cellpadding="0">
    <tbody>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 25%;" width="25%">Elaboracion de los logotipos en AutoCad</td>
            <td style="width: 25%;" colspan="2"><img src="../images/week03/cut01.jpg" alt="cut01.jpg" width="320" height="223" data-api-endpoint="../images/week03/cut01.jpg" data-api-returntype="File" /></td>
            <td style="width: 25%;"></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 25%;" rowspan="2">Separar los logotipos para abrir en el programa CutStudio</td>
            <td style="width: 25%;"><img src="../images/week03/cut02.jpg" alt="cut02.jpg" width="320" height="198" data-api-endpoint="../images/week03/cut02.jpg" data-api-returntype="File" /></td>
            <td style="width: 25%;"><img src="../images/week03/cut03.jpg" alt="cut03.jpg" width="320" height="200" data-api-endpoint="../images/week03/cut03.jpg" data-api-returntype="File" /></td>
            <td style="width: 25%;"><img src="../images/week03/cut04.jpg" alt="cut04.jpg" width="320" height="168" data-api-endpoint="../images/week03/cut04.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr>
            <td style="width: 25%;"><img src="../images/week03/cut06.jpg" alt="cut06.jpg" width="320" height="200" data-api-endpoint="../images/week03/cut06.jpg" data-api-returntype="File" /></td>
            <td style="width: 25%;"><img src="../images/week03/cut05.jpg" alt="cut05.jpg" width="320" height="190" data-api-endpoint="../images/week03/cut05.jpg" data-api-returntype="File" /></td>
            <td style="width: 25%;"></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 25%;" rowspan="2">Se abre por separado cada dise&ntilde;o en el programa CutStudio y se cambia los rollos de color para cada dise&ntilde;o</td>
            <td style="width: 25%;"><img src="../images/week03/cut7.jpg" alt="cut7.jpg" width="320" height="226" data-api-endpoint="../images/week03/cut07.jpg" data-api-returntype="File" /></td>
            <td style="width: 25%;"><img src="../images/week03/cut8.jpg" alt="cut8.jpg" width="320" height="226" data-api-endpoint="../images/week03/cut8.jpg" data-api-returntype="File" /></td>
            <td style="width: 25%;"><img src="../images/week03/cut9.jpg" alt="cut9.jpg" width="320" height="227" data-api-endpoint="../images/week03/cut9.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr>
            <td style="width: 25%;"><img src="../images/week03/cut10.jpg" alt="cut10.jpg" width="320" height="227" data-api-endpoint="../images/week03/cut10.jpg" data-api-returntype="File" /></td>
            <td style="width: 25%;"><img src="../images/week03/cut11.jpg" alt="cut11.jpg" width="320" height="227" data-api-endpoint="../images/week03/cut11.jpg" data-api-returntype="File" /></td>
            <td style="width: 25%;"><img src="../images/week03/cut12.jpg" alt="cut12.jpg" width="320" height="227" data-api-endpoint="../images/week03/cut12.jpg" data-api-returntype="File" /></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 25%;">Se obtienen las piezas recortadas</td>
            <td style="width: 25%;"></td>
            <td style="width: 25%;"></td>
            <td style="width: 25%;"></td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 25%;" rowspan="2">Se elabora la base de material acrilico de color blanco</td>
            <td style="width: 25%;">
                <p><img src="../images/week03/IMG_20230807_173927.jpg" alt="IMG_20230807_173927.jpg" width="320" height="144" data-api-endpoint="../images/week03/IMG_20230807_173927.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 25%;">
                <p><img style="font-family: inherit; font-size: 1rem;" src="../images/week03/IMG_20230807_173922.jpg" alt="IMG_20230807_173922.jpg" width="320" height="144" data-api-endpoint="../images/week03/IMG_20230807_173922.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width:25%;"></td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <p><img src="../images/week03/IMG_20230807_173916.jpg" alt="IMG_20230807_173916.jpg" width="320" height="144" data-api-endpoint="../images/week03/hIMG_20230807_173916.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 25%;">
                <p><img src="../images/week03/IMG_20230807_173908.jpg" alt="IMG_20230807_173908.jpg" width="320" height="144" data-api-endpoint="../images/week03/IMG_20230807_173908.jpg" data-api-returntype="File" /></p>
            </td>
            <td style="width: 25%;">
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr style="height: 15.0pt;">
            <td style="height: 15pt; width: 100%;" colspan="4">Dise&ntilde;o terminado</td>
        </tr>
        <tr>
            <td style="width: 25%;"></td>
            <td style="width: 25%;"></td>
            <td style="width: 25%;"></td>
            <td style="width: 25%;"></td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<p>## Enlaces &uacute;tiles</p>
<ul>
    <li><a class="inline_disabled" title="Enlace" href="https://www.autodesk.com/products/fusion-360/overview?term=1-YEAR&amp;tab=subscription" target="_blank" rel="noopener">Fusion 360</a></li>
    <li><a class="inline_disabled" href="https://rdworkslab.com/" target="_blank" rel="noopener">RDworks</a></li>
    <li><a class="inline_disabled" href="https://www.rolanddga.com/es/productos/software/roland-cutstudio-software" target="_blank" rel="noopener noreferrer">CutStudio</a></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>