# 6. Molding and casting

In this activity, a silicone mold of a specific object was obtained, with this mold an exact copy of the object was made but in resin. At the same time we also made the matrix for another mold but with 3D design and CNC manufacturing.

## Molding from a real object.

![](../images/tarea6/t6-00.jpg)

Object to be used as a molding base.

![](../images/tarea6/t6-01.jpg)

Construction of the molding chamber according to the size of the object.

![](../images/tarea6/t6-02.jpg)

Insertion of industrial plasticine as first layer and base. The object is pressed on top.

![](../images/tarea6/t6-03.jpg)

Addition of release agent to the object and the plasticine.

![](../images/tarea6/t6-04.jpg)

The silicone to be used is F-20 Plus.

![](../images/tarea6/t6-05.jpg)

Calculate the volume of silicone to be used by adding the catalyst (3 grams of catalyst per 100 grams of silicone) and pour over the object and plasticine.

![](../images/tarea6/t6-06.jpg)

Allow to set and cure for a few hours.

![](../images/tarea6/t6-07.jpg)

Once dry, remove the plasticine and apply the release agent again.

![](../images/tarea6/t6-08.jpg)

Calculate the volume of silicone again and add the missing part. Let the mold rest and dry.

![](../images/tarea6/t6-09.jpg)

Mold with both sides complete.

![](../images/tarea6/t6-10.jpg)

Continue with the addition of resin to the mold. The resin to be used is Polyester Resin.

![](../images/tarea6/t6-11.jpg)

Join the molds by means of two plates and with the help of rubber bands.

![](../images/tarea6/t6-12.jpg)

Calculate the volume of resin, and pour it into the molds.

![](../images/tarea6/t6-13.jpg)

Let the molded object rest and dry.

![](../images/tarea6/t6-14.jpg)

Once dry, remove the molded object carefully.

![](../images/tarea6/t6-15.jpg)

Comparison between real object and molded object.

## Molding from a digital object.

![](../images/tarea6/t6-16.jpg)

Generation of the object in Fusion 360

![](../images/tarea6/t6-17.jpg)

Mold base generation in Fusion 360

<iframe width="1271" height="500" src="https://www.youtube.com/embed/Uj8zOP4rQUQ" title="Moldeo CNC" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Routing configuration for CNC machining

> The configuration of the CNC machining path is done by means of the "Adaptive Roughing" operation, in which the RPM, feed rate, tool penetration depth and everything related to the tool movement is configured.

![](../images/tarea6/t6-18.jpg)

G-code opening in V-Panel software.

![](../images/tarea6/t6-19.jpg)

The mold base will be manufactured in kerosene wax and with the Monofab Roland SRM-20.

<iframe width="452" height="803" src="https://www.youtube.com/embed/h0rMOu3lTyQ" title="VID 20230817 144343" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Machining process for the mold base.

![](../images/tarea6/t6-20.jpg)

Lower mold base.

![](../images/tarea6/t6-21.jpg)

Upper mold base.

![](../images/tarea6/t6-22.jpg)

Upper and lower mold base ready for molding.


## Explanation of the casting process

The following video will detail the whole process of casting the parts with the molds.

<iframe width="1271" height="500" src="https://www.youtube.com/embed/4qacbDKVvto" title="Moldeo fablab" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

![](../images/tarea6/t6-23.jpg)

Comparison between real object and molded object (manual molding and digital molding).

![](../images/tarea6/t6-24.jpg)

Both jobs were successfully completed!

## Problems encountered in the development and their solution

### Manual molding

The problems encountered in the manual molding process were: 

- The silicone might not completely fill the defined space, which would generate an irregular mold or simply be unusable.
- Using too much release paste would result in the molded object not having a very detailed finish.

The solution to these problems:

- It had to be ensured that the silicone drained completely into the base to avoid irregularities. 
- The release paste had to be applied with care and taking into account the details of the object to achieve a more even molded object.

### Digital molding

The problems presented in the digital molding process were: 

- In the machining process there was an excessive accumulation of excess chips which could affect the machining finish.
- At the end of the machining process shavings were adhered to the area where the mold was made, which would generate a poor finish.

The solution to these problems:

- Solution is given by making small pauses to the machining process to clean the excess shavings. 
- It is necessary to clean the base of the mold, eliminating all the remaining shavings very carefully, thus securing the mold that will be generated later.


## Useful links

- [Fusion 360](https://www.autodesk.com/products/fusion-360/overview?term=1-YEAR&tab=subscription)

- [V-Panel](https://www.rolanddga.com/es/productos/software/3d-bundle)


## Files

- [Mold base](../images/tarea6/files/moldeo-hoja.f3d)
