<!DOCTYPE html>
<html>
<head>
	<title>JOSE ANTONIO - FabLab2023</title>
	<meta charset="utf-8">
	<meta name="author" content="Jose Contreras Fab Lab">
	<meta name="description" content="HTML5/CSS3"/>
	<link rel="stylesheet" type="text/css" href="./assigments/css/reset.css">
	<link rel="stylesheet" type="text/css" href="./assigments/css/fancybox-thumbs.css">
	<link rel="stylesheet" type="text/css" href="./assigments/css/fancybox-buttons.css">
	<link rel="stylesheet" type="text/css" href="./assigments/css/fancybox.css">
	<link rel="stylesheet" type="text/css" href="./assignments/css/animate.css">
	<link rel="stylesheet" type="text/css" href="./assigments/css/main.css">

  <script type="text/javascript" src="./assigments/js/jquery.js"></script>
  <script type="text/javascript" src="./assigments/js/fancybox.js"></script>
  <script type="text/javascript" src="./assigments/js/fancybox-buttons.js"></script>
  <script type="text/javascript" src="./assigments/js/fancybox-media.js"></script>
  <script type="text/javascript" src="./assigments/js/fancybox-thumbs.js"></script>
  <script type="text/javascript" src="./assigments/js/wow.js"></script>
  <script type="text/javascript" src="./assigments/js/main.js"></script>
</head>
<body>

<section class="billboard light">
	<header class="wrapper light">
		<nav>
			<ul>
				<li><a href="#PROJECT">Project Proposal</a></li>
                                      <li><a href="#ABOUTME">About me</a></li>
				<li><a href="#CALENDAR">Calendar</a></li>
				<li><a href="#USEFULHINTS">Useful hints</a></li>
			</ul>
		</nav>
	</header>

<div class="caption light animated wow fadeInDown clearfix">
	<h1>JOSE ANTONIO</h1>
                       <p><em>Sometimes it takes more time to achieve our dreams</em></p>
			<hr>
		</div>
		<div class="shadow"></div>
	</section><!--  End billboard  -->


<section class="services wrapper">
	<ul class="clearfix">
		<li class="animated wow fadeInDown">
                              <img class="icon" src="img/project_boceto.jpg" alt=""/>
			<span class="separator"></span>
	<h2 id="PROJECT">Project Proposal</h2>
			<p>This project </p>
                                <p>vehicle.</p>
		</li>
	</ul>
</section><!--  End services  -->


<section class="video">
		<h3 class="animated wow fadeInDown">We enjoy the game.</h3>
        </section><!--  End video -->


<section class="testimonials wrapper">
	<div class="title animated wow fadeIn">
		<h2 id="ABOUTME">About me</h2>
                        <h3><em>Every day is an opportunity to make our world better</em></h3>
		<hr class="separator"/>
	</div>

<ul class="clearfix">
		<li class="animated wow fadeInDown">
                            <p> 
				<span class="triangle"></span>
				</p>
				<div class="client">
					<img src="img/WWWWWW.png" class="avatar"/>
					<div class="client_details">
						<h4>Jose Contreras</h4>
						<h5>Docente</h5>
					</div>
				</div>
			</li>
		</ul>
	</section><!--  End testimonials  -->


<section class="blog_posts">
		<div class="wrapper">
			<div class="title animated wow fadeIn">
				<h2 id="CALENDAR">Calendar</h2>
				<h3>Fab Lab schedule</h3>
				<hr class="separator"/>
			</div>

<ul class="clearfix">
				<li class="animated wow fadeInDown">
					<div class="media">
						<a href="./asignments/w1/week1.html" target="_blank">
							<img src="img/blog_post1.jpg" alt=""/>
						</a>
					</div>
                                    <a href="./asignments/w1/week1.html" target="_blank">
						<h1>Project Management - Week 1</h1>
					</a>
				</li>
                <li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<a href="./asignments/w2/week2.html" target="_blank">
							<img src="img/CAD.png" alt=""/>
						</a>
					</div>					
					<a href="./asignments/w2/week2.html" target="_blank">
						<h1>Computer Aided Design - Week 2</h1>
					</a>
				</li>
                <li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<a href="./asignments/w3/week3.html" target="_blank">
							<img src="img/pressfit1.png" alt=""/>
						</a>
					</div>					
					<a href="./asignments/w3/week3.html" target="_blank">
						<h1>Computer Controlled cutting - Week 3</h1>
					</a>
				</li>
				<li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<a href="./asignments/w4/week4.html" target="_blank">
							<img src="img/fabisp_pcb.png" alt=""/>
						</a>
					</div>					
					<a href="./asignments/w4/week4.html" target="_blank">
						<h1>Electronics Production - Week 4</h1>
					</a>
				</li>
                <li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<a href="./asignments/w5/week5.html" target="_blank">
							<img src="img/print_snoopy_2.jpg" alt=""/>
						</a>
					</div>					
					<a href="./asignments/w5/week5.html" target="_blank">
						<h1>3D scanning and printing - Week 5</h1>
					</a>
				</li>
				<li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<a href="./asignments/w6/week6.html" target="_blank">
							<img src="img/RenderFusion3.png" alt=""/>
						</a>
					</div>					
					<a href="./asignments/w6/week6.html" target="_blank">
						<h1>Electronics Design - Week 6</h1>
					</a>
				</li>
				<li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<a href="./asignments/w7/week7.html" target="_blank">
							<img src="img/TV_set_pine_color.png" alt=""/>
						</a>
					</div>					
					<a href="./asignments/w7/week7.html" target="_blank">
						<h1>Computer Controlled Machining - Week 7</h1>
					</a>
				</li>
                <li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<a href="./asignments/w8/week8.html" target="_blank">
							<img src="img/led_blink2.jpg" alt=""/>
						</a>
					</div>					
					<a href="./asignments/w8/week8.html" target="_blank">
						<h1>Embedded Programming - Week 8</h1>
					</a>
				</li>
                <li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<a href="./asignments/w9/week9.html" target="_blank">
							<img src="img/bone.jpg" alt=""/>
						</a>
					</div>					
					<a href="./asignments/w9/week9.html" target="_blank">
						<h1>Molding and casting - Week 9</h1>
					</a>
				</li>			
                <li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<a href="./asignments/w12/week12.html" target="_blank">
							<img src="img/hall_sensor4.png" alt=""/>
						</a>
					</div>					
					<a href="./asignments/w12/week12.html" target="_blank">
						<h1>Input devices - Week 12</h1>
					</a>
				</li>
                <li class="animated wow fadeInDown" data-wow-delay=".2s">
					<div class="media">
						<a href="./asignments/w13/week13.html" target="_blank">
							<img src="img/hbridge4.jpg" alt=""/>
						</a>
					</div>					
					<a href="./asignments/w13/week13.html" target="_blank">
						<h1>Output devices - Week 13</h1>
					</a>
				</li>				
			</ul>
		</div>
	</section><!--  End blog_posts  -->
    <script src='../ga.js'></script>
</body>
</html>