# 1. Principles and practices

## Assignment

Planning and outlining a possible final project.
This week I have read and understood the Fab Charter, also worked on defining my final project idea and started to get used to the documentation process.

## Fab Charter

I have understood the Fablab concept, I summarize it below:

Fablab labs are a global network, implemented in various educational institutions that provide access to digital fabrication tools.

These labs are made available as a community resource, the purpose is to help with tools in the creative process, the inventor chooses the use; it can be privately owned or open source. 
But remember this is a collaborative community, open source is recommended for the benefit of all.


## Final Project Proposal

The final project is "Drying and Weight Control Box for 3D Printer Filament."

![](../images/week00/proyect_skech.jpg)

### Justification

The filament of 3D printers being biodegradable products PLA, ABS, tend to absorb moisture from the environment which makes the final product does not get to have the desired quality, both in surface appearance and resistance or hardness.
When creating or printing parts it is also necessary to have control of how much material is available in order to avoid interruptions in the process.

### What will you do?

#### First: 
I will design the structure using a CAD program.
#### Second: 
I will section the structure in pieces for the dimensions of the 3D printer platform and laser cutter.
#### Third: 
I will simulate the assembly of the parts to determine the functionality of the moving parts.
#### Fourth: 
I will use equipment for the elaboration of the parts and components (3D printer, laser cutter).
#### Fifth: 
I will integrate in the design the electronics for the operation, weight control and temperature control of the drying chamber.

### Who will use it?

Companies and/or people dedicated to the design and elaboration of products in 3D printers.