#3. Computer Aided design
<p>This week I worked on defining my final project idea and started to getting used to the documentation process.</p>
<p>&nbsp;</p>
<p><span style="font-size: 14pt;"><strong>Research</strong></span></p>
<p>3D modeling can have a daunting learning curve, and most beginners don't know where to start. There are a variety of basic and advanced CAD programs that are commonly used to create digital objects. &nbsp; Such as SketchUp or Blender, which offer an overwhelming amount of features and control schemes that can lead anyone to give up before they even start.</p>
<p>There are many excellent programs, even from the maker of the best CAD software, available for free and geared specifically to those users who are new to 3D modeling.</p>
<table style="border-collapse: collapse; width: 100%; height: 1399px; border-style: none;" border="0">
    <tbody>
        <tr style="height: 0px;">
            <td style="width: 100%; height: 0px; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 50%; height: 29px; border-style: none;"><a class="inline_disabled" href="https://www.tinkercad.com/" target="_blank" rel="noopener"><span style="font-size: 14pt;"><strong>Tinkercad</strong></span></a><br />With a simple, eye-catching interface and many educational resources, Tinkercad is designed specifically for those with no previous CAD experience. In addition to being one of the most accessible 3D modeling programs, it can be used directly in the browser.</td>
            <td style="width: 50%; height: 39px; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/tinkercad.png"
                 alt="tinkercad.png" width="640" height="181" data-api-endpoint="../images/week02/tinkercad.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: OBJ, SVG, STL, PART</td>
        </tr>
        <tr style="height: 0px;">
            <td style="width: 100%; height: 0px; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 50%; height: 29px; border-style: none;"><a class="inline_disabled" href="https://www.vectary.com/" target="_blank" rel="noopener"><span style="font-size: 14pt;"><strong>Vectary</strong></span></a><br />Vectary is a browser-based parametric and mesh modeling software that bills itself as "the easiest-to-use online 3D design and augmented reality tool". Certainly, its simple and easy to master interface manages to include some excellent features.</td>
            <td style="width: 50%; height: 39px; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/Vectary.png" alt="Vectary.png" width="320" height="166" data-api-endpoint="../images/week02/Vectary.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: OBJ, DAE, USDZ, GLB, gITF, STL</td>
        </tr>
        <tr style="height: 1px;">
            <td style="width: 100%; height: 0px; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 50%; height: 29px; border-style: none;"><a class="inline_disabled" href="http://www.meshmixer.com/" target="_blank" rel="noopener"><strong><span style="font-size: 14pt;">Meshmixer</span></strong></a><br />Meshmixer is a lightweight 3D modeling software developed by Autodesk that any creator should have. As the software's website states, it is a "Swiss Army Knife" for 3D objects that allows you to manipulate, add, splice, merge models, etc., with ease.</td>
            <td style="width: 50%; height: 39px; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/meshmixer.png" alt="meshmixer.png" width="320" height="173" data-api-endpoint="../images/week02/meshmixer.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: OBJ, STL, 3MF, AMF, DAE, PLY, WRL, SMESH</td>
        </tr>
        <tr style="height: 1px;">
            <td style="width: 100%; height: 0px; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 50%; height: 29px; border-style: none;"><a class="inline_disabled" href="https://stephaneginier.com/sculptgl/" target="_blank" rel="noopener"><strong><span style="font-size: 14pt;">SculptGL</span></strong></a><br />SculptGL is an open source, freely available, browser-based sculpting program. Like most sculpting programs, this software simply places a piece of digital clay on your workspace and allows you to mold it with various tools called "brushes".</td>
            <td style="width: 50%; height: 39px; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/sculpt.png" alt="sculpt.png" width="320" height="229" data-api-endpoint="../images/week02/sculpt.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: SGL, OBJ, PLY, STL</td>
        </tr>
        <tr style="height: 1px;">
            <td style="width: 100%; height: 0px; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 50%; height: 29px; border-style: none;"><a class="inline_disabled" href="https://www.maxon.net/es/zbrushcoremini" target="_blank" rel="noopener"><strong><span style="font-size: 14pt;">ZBrushCoreMini</span></strong></a><br />ZBrushCoreMini is a scaled-down version of Pixologic's ZBrush, a powerful sculpting program popular with advanced and professional designers. However, don't let that scare you away. CoreMini is intended for beginner users, making it easy to learn for those with little 3D modeling experience.</td>
            <td style="width: 50%; height: 39px; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/Zbrush.png" alt="Zbrush.png" width="320" height="171" data-api-endpoint="../images/week02/Zbrush.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: OBJ</td>
        </tr>
        <tr style="height: 1px;">
            <td style="width: 100%; height: 0px; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 29px;">
            <td style="width: 50%; height: 29px; border-style: none;"><a class="inline_disabled" href="https://www.sketchup.com/es/plans-and-pricing/sketchup-free" target="_blank" rel="noopener"><span style="font-size: 14pt;"><strong>SketchUp Free</strong></span></a><br />SketchUp is a CAD drawing and extrusion software that is considered one of the best professional programs on the market. The free version, SketchUp Free, is an excellent browser-based tool within the reach of novice users.</td>
            <td style="width: 50%; height: 39px; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/skechUp.png" alt="skechUp.png" width="320" height="180" data-api-endpoint="../images/week02/skechUp.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: 3DS, DWG, DXF, FBX, KMZ, OBJ, VRML, XSI, SKP, PNG, STL</td>
        </tr>
        <tr style="height: 1px;">
            <td style="height: 0px; width: 100%; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;"><a class="inline_disabled" href="http://www.wings3d.com/" target="_blank" rel="noopener"><strong><span style="font-size: 14pt;">Wings 3D</span></strong></a><br />Wings 3D is an open source subdivision modeler (also known as a mesh modeler) that offers advanced tools, yet is not so intimidating as to scare off new users.</td>
            <td style="height: 20px; width: 50%; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/wings 3d.png" alt="wings 3d.png" width="320" height="252" data-api-endpoint="../images/week02/wings 3d.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: NDO, 3DS, BZW, DAE, GLB, EPS, SVG, JSCAD, IWO, IXO, OBJ, RWX, STL, WRL, X</td>
        </tr>
        <tr style="height: 1px;">
            <td style="width: 100%; height: 0px; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 222px;">
            <td style="width: 50%; height: 222px; border-style: none;"><a class="inline_disabled" href="https://makers.leopoly.com/" target="_blank" rel="noopener"><span style="font-size: 14pt;"><strong>Leopoly</strong></span></a><br />Leopoly is a free browser-based sculpting program that offers a very simple selection of tools. Its simplicity makes it an excellent entry point for beginners and students who want to learn to master the art of creating, customizing and 3D printing their own models.</td>
            <td style="width: 50%; height: 232px; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/leopoly.png" alt="leopoly.png" width="320" height="183" data-api-endpoint="../images/week02/leopoly.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: OBJ, STL</td>
        </tr>
        <tr style="height: 1px;">
            <td style="width: 100%; height: 0px; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 217px;">
            <td style="width: 50%; height: 217px; border-style: none;"><a class="inline_disabled" href="https://www.blockscad3d.com/" target="_blank" rel="noopener"><strong><span style="font-size: 14pt;">BlocksCAD</span></strong></a><br />Like Tinkercad's implementation with code blocks, or "codeblocks", BlocksCAD is a 3D modeler that simplifies OpenSCAD scripting for novice users through a colorful and attractive interface.</td>
            <td style="width: 50%; height: 227px; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/blockcad.png" alt="blockcad.png" width="320" height="195" data-api-endpoint="../images/week02/blockcad.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: XML, SCAD</td>
        </tr>
        <tr style="height: 1px;">
            <td style="width: 100%; height: 0px; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 136px;">
            <td style="width: 50%; height: 136px; border-style: none;"><a class="inline_disabled" href="https://www.blender.org/" target="_blank" rel="noopener"><strong><span style="font-size: 14pt;">Blender</span></strong></a><br />Blender is a completely free and open source 3D modeling and animation program. It is one of the most powerful of its kind and is used by highly skilled professionals to create even animated feature films such as Netflix's Next Gen. It is available for free to all users and will cover all your needs, but its interface can be very intimidating.</td>
            <td style="width: 50%; height: 146px; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/blender.png" alt="blender.png" width="320" height="169" data-api-endpoint="../images/week02/blender.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: DAE, ABC, USDC, SVG, PDF, BVH, PLY, STL, FBX, GLB, OBJ, X3D</td>
        </tr>
        <tr style="height: 1px;">
            <td style="width: 100%; height: 0px; background-color: #3598db; border-style: none;" colspan="2"></td>
        </tr>
        <tr style="height: 207px;">
            <td style="width: 50%; height: 207px; border-style: none;"><a class="inline_disabled" href="https://latinoamerica.autodesk.com/products/fusion-360/overview?term=1-YEAR&amp;tab=subscription&amp;plc=F360" target="_blank" rel="noopener"><strong><span style="font-size: 14pt;">Fusion 360</span></strong></a><br />One of the most complete design programs but also one of the most difficult to learn and use is Fusion 360. &nbsp; We can create 3D solids, and take them to manufacturing machines (3D printers, CNC, etc).</td>
            <td style="width: 50%; height: 217px; border-style: none;" rowspan="2">
                <p>&nbsp;</p>
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="../images/week02/fusion.png" alt="fusion.png" width="320" height="180" data-api-endpoint="../images/week02/fusion.png" data-api-returntype="File" /></p>
            </td>
        </tr>
        <tr style="height: 10px;">
            <td style="width: 50%; height: 10px; border-style: none;">Formatos: f3d , f3z , fsch , fbrd , flbr</td>
        </tr>
        <tr style="height: 0px;">
            <td style="border-style: none; width: 100%; height: 0px; background-color: #3598db;" colspan="2"></td>
        </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<p><span style="font-size: 14pt;"><strong>Development</strong></span></p>
<p>For practical purposes I will use the FUSION 360 program, since several of the tools and equipment we have in the FabLab are compatible with the program.</p>
<p>To do so, I will first do a search on Youtube to understand the use of the program. &nbsp; Several videos were found, most of them copyrighted.</p>
<p>Second we searched the Autodesk web site and found a tutorial course for the use of the program, we put the link to the page.</p>
<p><a class="inline_disabled" title="Enlace" href="https://help.autodesk.com/view/fusion360/ENU/courses/" target="_blank" rel="noopener noreferrer">Tutoriales FUSION 360 - Autodesk</a></p>
