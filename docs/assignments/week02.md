# 2. Project management

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Preparation of the platform

To be able to work it is necessary to install 2 programs:

### Sublime_Text
- [Sublime Text](https://www.sublimetext.com/)
The installation is simple, just press next until the installation complete message appears.

![](../images/week01/paso01.jpg)
![](../images/week01/paso02.jpg)
![](../images/week01/paso03.jpg)

### Git
- [Git Lab](https://about.gitlab.com/)
The only thing you have to pay attention to is when it asks you to select an editor, in this case we will select Sublime_Text, then next until the installation complete message appears.

![](../images/week01/paso04.jpg)
![](../images/week01/paso05.jpg)
![](../images/week01/paso06.jpg)
![](../images/week01/paso07.jpg)


