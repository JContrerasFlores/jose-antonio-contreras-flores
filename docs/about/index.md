# About me

![](../images/jcontreras2023.jpg)

Hi, I'm Jose Contreras. I am a teacher in the area of Mechanics in Tecsup Sur, resident in Arequipa, Perú, my career line is Design, with studies in Project Management, Mechanical Design and Design of Elements and Mechanisms.

Visit this website to see my work!

## My background

I was born in a beautiful city called Arequipa, in southern Peru.   It is the second largest city in the country.
The climate is sunny most of the year, with many touristic places and a great gastronomy represented in typical dishes.

## Previous work

In the courses I teach, I motivate students to develop projects in which they integrate the knowledge acquired in their previous and current courses, with the purpose of strengthening and improving their understanding by applying their knowledge on real machines and equipment.

### Project A

These images correspond to the design of a Press of Union and Vulcanizing of Material Conveyor Belt:

- Real application of the press.
![](..\images\week00\VM-0301-FE.jpg)

- Images of the development of the project.
![](..\images\week00\prensa.jpg)
![](..\images\week00\prensa02.jpg)
![](..\images\week00\prensa01.jpg)

